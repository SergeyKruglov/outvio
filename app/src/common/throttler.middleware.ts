import { NextFunction, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Env } from './env.helper';
import { RequestsService } from '../requests/requests.service';
import { User } from '../users/users.entity';

export function throttlerMiddleware(weight: string = 'WEIGHT_1'): Function {
  return async (req: Request, res: Response, next: NextFunction) => {
    const xForwarderFor: any = req.headers['x-forwarded-for'];
    const xRemoteAddr: any = req.headers['x-remote-addr'];

    const reg = /\s*,\s*/;

    let ip: string = xForwarderFor.split(reg)[0];
    if (!ip) ip = xRemoteAddr.split(reg)[0];

    console.log({ xForwarderFor, xRemoteAddr });
    console.log({ ip });

    let user: User | undefined;
    const token = req.query.token as string;
    if (token) {
      const usersRepository = getRepository(User);
      user = await usersRepository.findOne({ token });
    }

    const limitKey = user ? `LIMIT_${user.rateLimitStep}` : 'LIMIT_1';
    const limit = Env.getNumber(limitKey);
    const reqWeight = Env.getNumber(weight);

    const now = new Date();
    const hourStart = new Date(
      now.getFullYear(),
      now.getMonth(),
      now.getDate(),
      now.getHours(),
    );

    const hourEnd = new Date(hourStart.getTime() + (60 * 60 * 1000));

    console.log({ hourStart, hourEnd });

    const requestService = new RequestsService();
    const requests = await requestService.findInThisHour(hourStart, hourEnd, ip, user);

    let requestsCount = reqWeight;
    for (const request of requests) {
      requestsCount += request.weight;
    }

    console.log({
      reqWeight,
      limit,
      requestsCount,
    });

    if (requestsCount > limit) {
      res.status(403).json({
        title: 'Rate limit exceeded!',
        message: `You can try again at ${hourEnd.toUTCString()}`,
      });

      return null;
    }

    await requestService.create(reqWeight, ip, user);

    next();
  };
}
