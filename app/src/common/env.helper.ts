export class Env {
  static get(key: string): string {
    const value = process.env[key];
    if (!value) {
      throw new Error(`Incorrect env param: ${key}`);
    }

    return value;
  }

  static getNumber(key: string): number {
    return Number(Env.get(key));
  }
}
