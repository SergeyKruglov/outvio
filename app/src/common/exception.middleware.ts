import { ExpressErrorMiddlewareInterface, Middleware } from 'routing-controllers';
import { Request, NextFunction, Response } from 'express';

@Middleware({ type: 'after' })
export class ExceptionMiddleware implements ExpressErrorMiddlewareInterface {
  error(err: any, req: Request, res: Response, next: NextFunction) {
    const code = err.httpCode;

    if (code) {
      res.status(code).send(err);
    } else {
      res.status(500).send(err);
    }

    next();
  }
}
