import {
  Controller, Get, Param, Post, Put, Delete, BodyParam as Body, HttpError,
} from 'routing-controllers';
import { UsersService } from './users.service';
import { User } from './users.entity';

@Controller('/users')
export class UsersController {
  private usersService: UsersService

  constructor() {
    this.usersService = new UsersService();
  }

  @Get()
  findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Get('/:id')
  async findOne(@Param('id') id: string) {
    const user = await this.usersService.findOne({ id: +id });
    if (!user) {
      throw new HttpError(404, 'Not found!');
    }

    return user;
  }

  @Post()
  create(@Body('rateLimitStep') rateLimitStep: number) {
    if (!rateLimitStep && rateLimitStep < 3 && rateLimitStep >= 1) {
      throw new HttpError(400, 'rateLimitStep is incorrect!');
    }

    return this.usersService.create(rateLimitStep);
  }

  @Put('/:id')
  async update(
    @Param('id') id: string,
    @Body('rateLimitStep') rateLimitStep: number,
  ) {
    if (!rateLimitStep && rateLimitStep < 3 && rateLimitStep >= 1) {
      throw new HttpError(400, 'rateLimitStep is incorrect!');
    }

    const user = await this.usersService.update(+id, rateLimitStep);
    if (!user) {
      throw new HttpError(404, 'Not found!');
    }

    return user;
  }

  @Delete('/:id')
  async delete(@Param('id') id: string) {
    const user = await this.usersService.delete(+id);
    if (!user) {
      throw new HttpError(404, 'Not found!');
    }

    return user;
  }
}
