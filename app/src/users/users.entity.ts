import {
  Column, Entity, OneToMany, PrimaryGeneratedColumn,
} from 'typeorm';
import { Request } from '../requests/requests.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  token: string;

  @Column({
    name: 'rate_limit_step',
  })
  rateLimitStep: number;

  @OneToMany(() => Request, (request) => request.user)
  requests: Request[];
}
