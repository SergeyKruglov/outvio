import { v4 as uuidv4 } from 'uuid';
import { getConnection, getRepository, Repository } from 'typeorm';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { HttpError } from 'routing-controllers';
import { User } from './users.entity';
import { Request } from '../requests/requests.entity';

export class UsersService {
  private usersRepository: Repository<User>;

  constructor() {
    this.usersRepository = getRepository(User);
  }

  async create(rateLimitStep: number): Promise<User> {
    const user = new User();
    user.token = uuidv4();
    user.rateLimitStep = rateLimitStep;
    await this.usersRepository.save(user);

    return user;
  }

  async findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(conditions: FindConditions<User>): Promise<User | undefined> {
    return this.usersRepository.findOne(conditions);
  }

  async update(id: number, rateLimitStep: number): Promise<User | undefined> {
    const user = await this.usersRepository.findOne(id);
    if (user) {
      user.rateLimitStep = rateLimitStep;
      await this.usersRepository.save(user);
      return user;
    }
  }

  async delete(id: number): Promise<User | undefined> {
    const user = await this.usersRepository.findOne(id);
    if (user) {
      const connection = getConnection();
      const queryRunner = connection.createQueryRunner();

      await queryRunner.connect();
      await queryRunner.startTransaction();

      try {
        await queryRunner.manager.delete(Request, { user });
        await queryRunner.manager.remove(user);
        await queryRunner.commitTransaction();

        return user;
      } catch (err) {
        await queryRunner.rollbackTransaction();
        throw new HttpError(500, 'Error while deleting!');
      } finally {
        await queryRunner.release();
      }
    }
  }
}
