import 'reflect-metadata';
import express, { Express } from 'express';
import { createConnection } from 'typeorm';
import { useExpressServer } from 'routing-controllers';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import compression from 'compression';
import { Env } from './common/env.helper';
import { OrmConfig } from './ormconfig';
import { UsersController } from './users/users.controller';
import { RequestsController } from './requests/requests.controller';
import { ExceptionMiddleware } from './common/exception.middleware';

async function init() {
  const host = Env.get('HOST');
  const port = Number(Env.get('PORT'));

  try {
    await createConnection(OrmConfig.get());
  } catch (err) {
    console.log(err);
  }

  const app: Express = express();
  app.use(helmet());
  app.use(bodyParser.json());
  app.use(compression());

  useExpressServer(app, {
    cors: true,
    controllers: [
      UsersController,
      RequestsController,
    ],
    middlewares: [
      ExceptionMiddleware,
    ],
    defaultErrorHandler: false,
  });

  app.listen(port, host, () => {
    console.log(`Server is listening on ${host}:${port}!`);
  });
}

init();
