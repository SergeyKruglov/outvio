import { ConnectionOptions } from 'typeorm';
import { Env } from './common/env.helper';
import { User } from './users/users.entity';
import { Request } from './requests/requests.entity';

export class OrmConfig {
  public static get(): ConnectionOptions {
    return {
      type: 'postgres',
      host: Env.get('POSTGRES_HOST'),
      port: Number(Env.get('POSTGRES_PORT')),
      username: Env.get('POSTGRES_USER'),
      password: Env.get('POSTGRES_PASSWORD'),
      database: Env.get('POSTGRES_DB'),
      entities: [User, Request],
      synchronize: true,
    };
  }
}
