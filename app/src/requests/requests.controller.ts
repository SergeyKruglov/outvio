import {
  Controller, Delete, Get, UseBefore,
} from 'routing-controllers';
import { throttlerMiddleware as throttler } from '../common/throttler.middleware';
import { Env } from '../common/env.helper';
import { RequestsService } from './requests.service';

@Controller('/endpoints')
export class RequestsController {
  private requestsService: RequestsService;

  constructor() {
    this.requestsService = new RequestsService();
  }

  @Get('/1')
  @UseBefore(throttler('WEIGHT_1'))
  endpoint1() {
    return { weight: Env.getNumber('WEIGHT_1') };
  }

  @Get('/2')
  @UseBefore(throttler('WEIGHT_2'))
  endpoint2() {
    return { weight: Env.getNumber('WEIGHT_2') };
  }

  @Get('/3')
  @UseBefore(throttler('WEIGHT_3'))
  endpoint3() {
    return { weight: Env.getNumber('WEIGHT_3') };
  }

  @Get('/4')
  @UseBefore(throttler('WEIGHT_2'))
  endpoint4() {
    return { weight: Env.getNumber('WEIGHT_2') };
  }

  @Get('/5')
  @UseBefore(throttler('WEIGHT_1'))
  endpoint5() {
    return { weight: Env.getNumber('WEIGHT_1') };
  }

  @Delete()
  async delete() {
    return { status: await this.requestsService.delete() };
  }
}
