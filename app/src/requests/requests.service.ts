import { Brackets, getRepository, Repository } from 'typeorm';
import { Request } from './requests.entity';
import { User } from '../users/users.entity';

export class RequestsService {
  private requestsRepository: Repository<Request>;

  constructor() {
    this.requestsRepository = getRepository(Request);
  }

  async create(weight: number, ip: string, user?: User): Promise<Request> {
    const request = new Request();
    request.weight = weight;
    request.ip = ip;
    request.user = user || null;
    await this.requestsRepository.save(request);

    return request;
  }

  findInThisHour(hourStart: Date, hourEnd: Date, ip: string, user?: User): Promise<Request[]> {
    return this.requestsRepository.createQueryBuilder('request')
      .where('request.created_at >= :hourStart', { hourStart })
      .andWhere('request.created_at < :hourEnd', { hourEnd })
      .andWhere('request.ip = :ip', { ip })
      .andWhere(new Brackets((qb) => {
        qb.where('request.user_id IS NULL');

        if (user) {
          qb.orWhere('request.user_id = :userId', { userId: user.id });
        }
      }))
      .getMany();
  }

  async delete() {
    await this.requestsRepository.clear();
    return 'success';
  }
}
