module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
  },
  plugins: [
    '@typescript-eslint',
  ],
  rules: {
    'import/prefer-default-export': 0,
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'class-methods-use-this': 0,
    'no-console': 0,
    'no-undef': 0,
    'prefer-destructuring': 0,
    'no-restricted-syntax': 0,
    'consistent-return': 0,
  },
};
