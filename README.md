
# Sergey Kruglov

### Run the app

```bash
$ docker-compose up
```

### App link

[http://localhost:57080](http://localhost:57080)

### PostgreSQL

**Host**: localhost      
**Port**: 57432     
**User**: user        
**Password**: VeryStrongPassword    

# cURL requests

## Users

Methods of this controller don't use throttler middleware because they are handy for testing.  
The rateLimitStep parameter is parts of 100/500/1000.
The first step is 100. The second is 500. The third is 1000.

#### Create

```bash
curl -X POST -H "Content-Type: application/json" -d '{"rateLimitStep": 1}' 'http://localhost:57080/users'
```

#### Get all 

```bash
curl 'http://localhost:57080/users'
```

#### Get one

```bash
curl 'http://localhost:57080/users/1'
```

#### Update

```bash
curl -X PUT -H "Content-Type: application/json" -d '{"rateLimitStep": 2}' 'http://localhost:57080/users/1'
```

#### Delete

```bash
curl -X DELETE 'http://localhost:57080/users/1'
```

## Endpoints

To avoid auth implementation, every user contains an "access" token.   
You can pass the token to **token** query param like this.

```bash
curl 'http://localhost:57080/endpoints/1?token=edc40a33-ff41-4b4b-b612-d42ec0da4750'
```

#### 1) Weight=1

```bash
curl 'http://localhost:57080/endpoints/1'
```

#### 2) Weight=2

```bash
curl 'http://localhost:57080/endpoints/2'
```

#### 3) Weight=5

```bash
curl 'http://localhost:57080/endpoints/3'
```

#### 4) Weight=2

```bash
curl 'http://localhost:57080/endpoints/4'
```

#### 5) Weight=1

```bash
curl 'http://localhost:57080/endpoints/5'
```

### Empty request history

```bash
curl -X DELETE 'http://localhost:57080/endpoints'
```
